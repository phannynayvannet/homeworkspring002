package com.example.demo.repository;

import com.example.demo.model.Student;
import com.example.demo.service.StudentService;
import com.example.demo.service.imp.StudentServiceImp;
import com.github.javafaker.Faker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StudentRepository {

    List<Student>  studentList = new ArrayList<>();

    public StudentRepository(){
        Faker faker = new Faker();
        for (int i=1; i <= 5; i++){
            Student  student = new Student();
            student.setId(i);
            student.setTitle(faker.name().fullName());
            student.setImage("Male");
            student.setDescription(faker.address().streetAddress());
        studentList.add(student);
        }
    }

    public List<Student> getAllStudent(){
        return studentList;
    }

    public boolean insertNew(Student student){
        studentList.add(student);
        return true;
    }
}
