package com.example.demo.model;

import lombok.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student {
    private int id;
    private String title;
    private String image;
    private String description;
 
}
