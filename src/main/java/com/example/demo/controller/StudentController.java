package com.example.demo.controller;

import com.example.demo.model.Student;
import com.example.demo.repository.StudentRepository;
import com.example.demo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

@Controller
public class StudentController {

    private StudentService studentService;
    @Autowired
    public void setStudentService(StudentService studentService){
        this.studentService=studentService;
    }
    @GetMapping("")
    public String viewIndex(ModelMap modelMap){
        modelMap.addAttribute("studentList",studentService.getAll());
        return "index";
    }
    @GetMapping("/show-form-add")
    public String viewFormAdd(){
        return "form-add";
    }
    @PostMapping("/add-new")
    public  String handleAdd(@ModelAttribute Student student){
        int id = studentService.getAll().size()+1;
        student.setId(id);
        studentService.insertNew(student);
        return "form-add";
    }
    @RequestMapping("/save")
    public String index()
    {
        return"index";
    }
    @RequestMapping(value="/save", method= RequestMethod.POST)
    public ModelAndView save(@ModelAttribute Student student)
    {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("index");
        modelAndView.addObject("Student", student);
        return modelAndView;
    }
    @PostMapping("/upload")
    public String fileMethod(@RequestParam("file") MultipartFile file) {
        try {
            String filename = file.getOriginalFilename();
            Path path = Paths.get("src/main/resources/file/" + filename);
            Files.copy(file.getInputStream(), path, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "index";
    }
}
