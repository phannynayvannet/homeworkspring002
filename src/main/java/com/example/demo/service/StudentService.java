package com.example.demo.service;

import com.example.demo.model.Student;

import java.util.List;

public interface StudentService {
    public List<Student> getAll();
    public Boolean insertNew(Student student);

}
